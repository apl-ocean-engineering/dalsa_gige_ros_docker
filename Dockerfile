ARG BUILD_TYPE=clone
ARG ROS_VERSION=noetic
ARG WS_DIR=/root/dalsa_ws

## This should work through the magic of multi-arch?
FROM ros:${ROS_VERSION}-perception AS build_image

# This should be set automatically by the build engine
ARG TARGETPLATFORM
ARG DALSA_SDK_BASE=https://s3.us-west-1.wasabisys.com/marburg/DalsaGigE_SDK
ARG DALSA_SDK_FILE=GigE-V-Framework_2.20.0.0182.tar.gz

RUN apt-get update && \
  apt-get install -y \
  git \
  libcap-dev \
  python3-catkin-tools \
  python3-vcstool \
  ros-noetic-diagnostic-updater \
  ros-noetic-roslint \
  wget \
  && \
  apt autoremove -y && \
  apt clean -y && \
  rm -rf /var/lib/apt/lists/*

WORKDIR /tmp
ENV DALSA_SDK_URL=${DALSA_SDK_BASE}/${DALSA_SDK_FILE}
RUN echo "Downloading ${DALSA_SDK_URL}" && \
    wget $DALSA_SDK_URL && \
    tar -xzvf ${DALSA_SDK_FILE} && \
    cd GigE-V-Framework_* && \
    if [ "${TARGETPLATFORM}" = "linux/amd64" ]; then \
      tar -xzv -C /root -f GigE-V-Framework_x86_*.tar.gz; \
    elif [ "${TARGETPLATFORM}" = "linux/arm64" ]; then \
      tar -xzv -C /root -f GigE-V-Framework_aarch64_*.tar.gz; \
    else \
      echo "Unknown target platform ${TARGETPLATFORM}"; \
      exit; \
    fi && \
    cd /root/DALSA/GigeV/bin && \
    TDY_INSTALL_MODE=Silent ./install.gigev Install && \
    rm -r /tmp/GigE-V*

# Install a default ROS entrypoint
WORKDIR /root
COPY ros_entrypoint.sh /root/ros_entrypoint.sh
ENTRYPOINT [ "/root/ros_entrypoint.sh" ]

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#  This build path clones from github
FROM build_image as build_clone

ARG DALSA_DRIVER_REPO=https://gitlab.com/apl-ocean-engineering/dalsa_gige_driver
ARG DALSA_DRIVER_BRANCH=master
ARG DALSA_PROJECT_ID=18649864

ARG WS_DIR
ONBUILD WORKDIR ${WS_DIR}/src
# This should invalidate cache when the repo changes
ADD "https://gitlab.com/api/v4/projects/${DALSA_PROJECT_ID}/repository/branches/master" /tmp/devalidateCache
ONBUILD RUN echo "Cloning from ${DALSA_DRIVER_BRANCH} branch ${DALSA_DRIVER_REPO}"
ONBUILD RUN git clone --depth 1 -b ${DALSA_DRIVER_BRANCH} ${DALSA_DRIVER_REPO}

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# This build path copies from a local tree
#
# I find the structure of the arena_camera_ros drive extremely annoying and
# can see changing away from it.
FROM build_image as build_local

# The local directory for the repo
ARG WS_DIR
ARG LOCAL_SRC=local_src
ONBUILD RUN echo "Using local directory ${LOCAL_SRC} for source"
ONBUILD COPY ${LOCAL_SRC} ${WS_DIR}/src/

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Then the two build paths re-merge here
# dockerfile_lint - ignore
FROM build_${BUILD_TYPE}
LABEL Version=0.1
LABEL Name=dalsa_gige_ros_docker

ARG ROS_VERSION

WORKDIR ${WS_DIR}/src
RUN vcs import < dalsa_gige_driver/dalsa_gige_driver.rosinstall

## Install dalsa_gige_grant
RUN git clone https://gitlab.com/apl-ocean-engineering/dalsa_gige_grant.git
WORKDIR ${WS_DIR}/src/dalsa_gige_grant/build
RUN cmake .. && \
    make -j && \
    install --owner=root --group=root --mode=4755 dalsa_gige_grant /usr/local/bin


WORKDIR ${WS_DIR}
RUN bash -c "/root/ros_entrypoint.sh \
                catkin build --cmake-args -DCMAKE_BUILD_TYPE=Release"

ENV CATKIN_WS ${WS_DIR}
CMD ["roslaunch", "dalsa_gige_driver", "mono_camera.launch"]
